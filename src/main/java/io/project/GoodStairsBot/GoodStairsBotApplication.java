package io.project.GoodStairsBot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoodStairsBotApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoodStairsBotApplication.class, args);
	}

}
